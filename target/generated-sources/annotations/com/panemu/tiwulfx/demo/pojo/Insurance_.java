package com.panemu.tiwulfx.demo.pojo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Insurance.class)
public abstract class Insurance_ {

	public static volatile SingularAttribute<Insurance, Integer> id;
	public static volatile ListAttribute<Insurance, Person> personList;
	public static volatile SingularAttribute<Insurance, String> name;
	public static volatile SingularAttribute<Insurance, String> code;

}

