/*
 * License BSD License
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.control.DetachableTabPane;
import com.panemu.tiwulfx.control.NumberField;
import com.panemu.tiwulfx.control.sidemenu.SideMenu;
import com.panemu.tiwulfx.control.sidemenu.SideMenuActionHandler;
import com.panemu.tiwulfx.control.sidemenu.SideMenuCategory;
import com.panemu.tiwulfx.control.sidemenu.SideMenuItem;
import com.panemu.tiwulfx.demo.misc.DataGenerator;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class FrmMain extends VBox {

    @FXML 
    private Button btnGenerate; 
    @FXML
    private DetachableTabPane tabpane;
    @FXML 
    private NumberField<Integer> txtRecordCount;
    @FXML
    private SideMenu sideMenu;

    public FrmMain() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FrmMain.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
//        tabInForm.setContent(new FrmPersonTable2(new InFormMaintenanceController()));
//        tabMasterDetail.setContent(new FrmMasterDetail());
        showAsTab(new FrmPersonTable(new InRowMaintenanceController()), "Edit in Row");
//		showAsTab(new FrmPersonTable2(new InFormMaintenanceController()), "Edit in Row");
        txtRecordCount.setNumberType(Integer.class);
        btnGenerate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (txtRecordCount.getValue() != null && txtRecordCount.getValue() > 0) {
                    DataGenerator.createWithTestData(txtRecordCount.getValue());
                    MessageDialogBuilder.info().message("message.record.has.been.generated", txtRecordCount.getValue()).show(FrmMain.this.getScene().getWindow());
                    txtRecordCount.setValue(null);
                }
            }
        });
		
		SideMenuCategory smCat = new SideMenuCategory("image-menu-cat1", "Editing");
		SideMenuItem smItem1 = new SideMenuItem("image-menu-item1", "Edit In Row", "showFrmPersonTable");
		SideMenuItem smItem2 = new SideMenuItem("image-menu-item2", "Edit In Form", "showFrmPersonTable2");
		smCat.addMainMenuItem(smItem1, smItem2);
		
		SideMenuItem smItem3 = new SideMenuItem("image-menu-item3", "Master Detail", "showFrmMasterDetail");
		SideMenuItem smItem4 = new SideMenuItem("image-menu-item4", "Tab Pane", "showFrmDetachableTabPane");
		sideMenu.addMenuItems(smCat, smItem3, smItem4);
		
		sideMenu.setActionHandler(new SideMenuActionHandler() {

			@Override
			public void executeAction(String actionName) {
				switch (actionName) {
					case "showFrmPersonTable" : showFrmPersonTable(); break;
					case "showFrmPersonTable2" : showFrmPersonTable2(); break;
					case "showFrmMasterDetail" : showFrmMasterDetail(); break;
					case "showFrmDetachableTabPane" : showFrmDetachableTabPane(); break;
				}
			}
		});
    }
    
    public void showAsTab(Pane frm, String label) {
        final Tab tab = new Tab(label);
        tab.setClosable(true);
        tab.setContent(frm);
        tabpane.getTabs().add(tab);
        tabpane.getSelectionModel().select(tab);
        
        /**
         * Workaround for TabPane memory leak
         */
        tab.setOnClosed(new EventHandler<Event>() {

            @Override
            public void handle(Event t) {
                tab.setContent(null);
            }
        });
        
    }   
    
    public void showFrmPersonTable() {
        showAsTab(new FrmPersonTable(new InRowMaintenanceController()), "Edit in Row");
    }
    
    public void showFrmPersonTable2() {
        showAsTab(new FrmPersonTable2(new InFormMaintenanceController()), "Edit in Form");
    }
    
    public void showFrmMasterDetail() {
        showAsTab(new FrmMasterDetail(), "Master Detail");
    }
	
	public void showFrmDetachableTabPane() {
        showAsTab(new FrmDetachableTabPane(), "Inner Scope");
    }
}
